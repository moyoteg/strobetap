//
//  StartStrobeTableViewController.m
//  StrobeTap
//
//  Created by Jaime Moises Gutierrez on 9/22/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import "StartStrobeTableViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "EZMicrophone.h"
#import "EZAudioPlot.h"

#import <AudioToolbox/AudioServices.h>

#define kMinimumPeriod 0.01
#define kMaximumPeriod 0.10

@interface StartStrobeTableViewController () <EZMicrophoneDelegate>

@property (strong, nonatomic) IBOutlet UIView *audioPlotView;

@property (strong, nonatomic) EZMicrophone *microphone;
@property (strong, nonatomic) EZAudioPlot *audioPlot;

@property (strong, nonatomic) IBOutlet UILabel *freqStrobeLabel;
@property (strong, nonatomic) IBOutlet UISlider *freqStrobeSlider;
@property (strong, nonatomic) IBOutlet UISwitch *freqStrobeLightSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *audioStrobeSwitch;
@property (strong, nonatomic) IBOutlet UIStepper *tempoStepper;
@property (strong, nonatomic) IBOutlet UIImageView *plotImageView;

@property (strong, nonatomic) IBOutlet NSMutableArray *audioDataArray;

@property NSTimeInterval strobeInterval;
@end

@implementation StartStrobeTableViewController

bool torchIsOn;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.audioDataArray = [[NSMutableArray alloc] init];
    
    torchIsOn = NO;
    
    self.strobeInterval = kMaximumPeriod;
    [self.freqStrobeSlider setMaximumValue:kMaximumPeriod];
    [self.freqStrobeSlider setMinimumValue:kMinimumPeriod];
    [self.freqStrobeSlider setValue:kMinimumPeriod];
    [self.freqStrobeSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [self toggleBrightness];
    }
}

- (IBAction)stepperValueChanged:(id)sender
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    NSLog(@"tempo stepper value: %f",self.tempoStepper.value);
    self.freqStrobeSlider.value = self.tempoStepper.value;
    self.freqStrobeLabel.text = [NSString stringWithFormat:@"tempo: %.2f", self.freqStrobeSlider.value * 60];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (void)sliderValueChanged:(UISlider *)sender
{
    NSLog(@"slider value = %f",sender.value);
    self.freqStrobeLabel.text = [NSString stringWithFormat:@"tempo: %.2f", self.freqStrobeSlider.value * 60];
    self.strobeInterval = self.freqStrobeSlider.value;
}

- (IBAction)pressedSwitch:(id)sender
{
    UISwitch *s = sender;
    if (s == self.freqStrobeLightSwitch)
    {
        [self stopAudioBasedStrobeLight];
        self.audioStrobeSwitch.on = NO;
        if (s.on)
        {
            [self startFrequencyBasedStrobeLight];
        }
        else
        {
            [self stopFrequencyBasedStrobeLight];
        }
    }
    else
    {
        [self stopFrequencyBasedStrobeLight];
        self.freqStrobeLightSwitch.on = NO;
        if (s.on)
        {
            [self startAudioBasedStrobeLight];
        }
        else
        {
            [self stopAudioBasedStrobeLight];
        }
    }
}

- (void)toggleTorch
{
    NSLog(@"torch toggled");
    torchIsOn = !torchIsOn;
    [self turnTorchOn:torchIsOn];
}

- (void)startFrequencyBasedStrobeLight
{
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerInterval) userInfo:nil repeats:NO];
}

- (void)startAudioBasedStrobeLight
{
    self.audioPlot = [[EZAudioPlot alloc] initWithFrame:self.plotImageView.frame];
    [self.plotImageView addSubview:self.audioPlot];
    
    // Background color (use UIColor for iOS)
    self.audioPlot.backgroundColor = [UIColor whiteColor];
    // Waveform color (use UIColor for iOS)
    self.audioPlot.color = [UIColor blackColor];
    // Plot type
    self.audioPlot.plotType = EZPlotTypeBuffer;
    // Fill
    self.audioPlot.shouldFill = YES;
    // Mirror
    self.audioPlot.shouldMirror = YES;
    
    self.microphone = [EZMicrophone microphoneWithDelegate:self];
    [self.microphone startFetchingAudio];
}

- (void)stopAudioBasedStrobeLight
{
    [self.microphone stopFetchingAudio];
}

-(void)updateTimerInterval
{
    if (self.freqStrobeLightSwitch.on)
    {
        [self toggleTorch];
        NSLog(@"strobe interval: %f",self.strobeInterval);
        [NSTimer scheduledTimerWithTimeInterval:self.strobeInterval target:self selector:@selector(updateTimerInterval) userInfo:nil repeats:NO];
    }
    else
    {
        [self stopFrequencyBasedStrobeLight];
    }
}

- (void)stopFrequencyBasedStrobeLight
{
    [self turnTorchOn:NO];
}

- (IBAction)touchedDownButton:(id)sender
{
    if (self.freqStrobeLightSwitch.on)
    {
        [self turnTorchOn:NO];
    }
    [self toggleTorch];
}

- (IBAction)pressedStart:(id)sender
{
    [self toggleTorch];
}

- (void)turnTorchOn:(bool)on
{
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    
    if (captureDeviceClass != nil)
    {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash])
        {
            
            [device lockForConfiguration:nil];
            if (on)
            {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                //torchIsOn = YES; //define as a variable/property if you need to know status
            }
            else
            {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                //torchIsOn = NO;
            }
            [device unlockForConfiguration];
        }
    }
}

#pragma mark - Microphone

/**
 The microphone data represented as float arrays useful for:
 - Creating real-time waveforms using EZAudioPlot or EZAudioPlotGL
 - Creating any number of custom visualizations that utilize audio!
 */
-(void)microphone:(EZMicrophone *)microphone hasAudioReceived:(float **)buffer withBufferSize:(UInt32)bufferSize withNumberOfChannels:(UInt32)numberOfChannels
{
    // Getting audio data as an array of float buffer arrays that can be fed into the EZAudioPlot, EZAudioPlotGL, or whatever visualization you would like to do with the microphone data.
    
    
    dispatch_async(dispatch_get_main_queue(),^{
        // Visualize this data brah, buffer[0] = left channel, buffer[1] = right channel
        
        [self.audioDataArray addObject:[NSNumber numberWithFloat:*buffer[0]]];
        
        if (self.audioDataArray.count == 64)
        {
            NSLog(@"audio data: %@", [self.audioDataArray description]);
            [self performSelectorOnMainThread:@selector(beat) withObject:nil waitUntilDone:NO];
            self.audioDataArray = [[NSMutableArray alloc] init];
        }
//        NSLog(@"updating audio buffer: %f",*buffer[0]);
//        if (*buffer[0] > 0.1)
//        {
//            [self performSelectorOnMainThread:@selector(beat) withObject:nil waitUntilDone:NO];
//        }
        [self.audioPlot updateBuffer:buffer[0] withBufferSize:bufferSize];
    });
}

- (void)vibrate
{
}

- (void)toggleBrightness
{
    if ([[UIScreen mainScreen] brightness] >= .5)
    {
        [self turnTorchOn:NO];
        [[UIScreen mainScreen] setBrightness:0.2];
    }
    else
    {
        [self turnTorchOn:YES];
        [[UIScreen mainScreen] setBrightness:1.0];
    }
}

- (void)beat
{
    [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(toggleBrightness) userInfo:nil repeats:NO];
}

@end