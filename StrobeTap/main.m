//
//  main.m
//  StrobeTap
//
//  Created by Jaime Moises Gutierrez on 9/22/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
