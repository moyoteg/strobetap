//
//  SettingsTableViewController.h
//  StrobeTap
//
//  Created by Jaime Moises Gutierrez on 10/8/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController

@end
