//
//  SettingsTableViewController.m
//  StrobeTap
//
//  Created by Jaime Moises Gutierrez on 10/8/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import "SettingsTableViewController.h"

#define kStrobeScreenBrightnessSettingKey @"kStrobeScreenBrightnessSettingKey"

@interface SettingsTableViewController ()
@property (strong, nonatomic) IBOutlet UISwitch *strobeScreenBrithnessSwitch;

@end

@implementation SettingsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initSettings];
}

- (void)initSettings
{
    self.strobeScreenBrithnessSwitch.on = [self LoadDefaultSetting:kStrobeScreenBrightnessSettingKey];
}

- (BOOL)LoadDefaultSetting:(NSString *)setting
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:setting];
}

- (IBAction)switchChangedValue:(id)sender
{
    UISwitch *s = sender;
    if (s == self.strobeScreenBrithnessSwitch)
    {
        [self toggleSetting:kStrobeScreenBrightnessSettingKey];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (void)toggleSetting:(NSString *)setting
{
    BOOL settingState = [[NSUserDefaults standardUserDefaults] boolForKey:setting];
    settingState = !settingState;
    [self saveDefaultSetting:setting withState:settingState];
}

- (void)saveDefaultSetting:(NSString *)setting withState:(BOOL)state
{
    [[NSUserDefaults standardUserDefaults] setBool:state forKey:setting];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
